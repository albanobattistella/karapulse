<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2019 Guillaume Desmottes 2019 <guillaume@desmottes.be -->
<component type="desktop-application">
  <id>@app-id@</id>
  <metadata_license>CC0</metadata_license>
  <project_license>GPL-3.0+</project_license>
  <name>Karapulse</name>
  <summary>Karaoke player with built-in web remote client</summary>
  <description>
    <p>Karapulse is a Linux karaoke player supporting CDG/MP3 as well as video files. It provides a self-served web application that singers can use with their phone to search for and queue their favorite songs.</p>
    <p>Once installed you'll need to index your songs collection. This can take a while if you have a lot of songs but needs to be done only once.
       To do so run the following command with the directory containing your songs ($HOME/karaoke in the example):</p>
    <p>flatpak run --command=karapulse-manage-db org.karapulse.Karapulse add-dir $HOME/karaoke</p>
  </description>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.freedesktop.org/gdesmott/karapulse/raw/master/data/screenshots/playback-video.png</image>
      <caption>Player rendering video</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.freedesktop.org/gdesmott/karapulse/raw/master/data/screenshots/playback-cdg.png</image>
      <caption>Player rendering CDG</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.freedesktop.org/gdesmott/karapulse/raw/master/data/screenshots/mobile-search.png</image>
      <caption>Searching song using the mobile client</caption>
    </screenshot>
  </screenshots>
  <url type="homepage">http://www.karapulse.org</url>
  <url type="bugtracker">https://gitlab.freedesktop.org/gdesmott/karapulse/issues</url>
  <categories>
    <category>Audio</category>
    <category>Video</category>
  </categories>
  <developer_name>Guillaume Desmottes</developer_name>
  <update_contact>gdesmott@gnome.org</update_contact>
  <translation type="gettext">@gettext-package@</translation>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <releases>
    <release version="0.2.0" date="2019-11-15">
      <description>
        <p>
          <ul>
            <li>web: synchronize play-pause button with server status</li>
            <li>Add translation support (fr)</li>
            <li>Hide cursor in fullscreen</li>
            <li>Fix queue restoring</li>
          </ul>
        </p>
      </description>
    </release>
    <release version="0.1.3" date="2019-08-29"/>
    <release version="0.1.2" date="2019-08-27"/>
    <release version="0.1.1" date="2019-08-27"/>
    <release version="0.1.0" date="2019-08-25"/>
  </releases>
  <content_rating type="oars-1.1">
    <content_attribute id="violence-cartoon">none</content_attribute>
    <content_attribute id="violence-fantasy">none</content_attribute>
    <content_attribute id="violence-realistic">none</content_attribute>
    <content_attribute id="violence-bloodshed">none</content_attribute>
    <content_attribute id="violence-sexual">none</content_attribute>
    <content_attribute id="violence-desecration">none</content_attribute>
    <content_attribute id="violence-slavery">none</content_attribute>
    <content_attribute id="violence-worship">none</content_attribute>
    <content_attribute id="drugs-alcohol">none</content_attribute>
    <content_attribute id="drugs-narcotics">none</content_attribute>
    <content_attribute id="drugs-tobacco">none</content_attribute>
    <content_attribute id="sex-nudity">none</content_attribute>
    <content_attribute id="sex-themes">none</content_attribute>
    <content_attribute id="sex-homosexuality">none</content_attribute>
    <content_attribute id="sex-prostitution">none</content_attribute>
    <content_attribute id="sex-adultery">none</content_attribute>
    <content_attribute id="sex-appearance">none</content_attribute>
    <content_attribute id="language-profanity">none</content_attribute>
    <content_attribute id="language-humor">none</content_attribute>
    <content_attribute id="language-discrimination">none</content_attribute>
    <content_attribute id="social-chat">none</content_attribute>
    <content_attribute id="social-info">none</content_attribute>
    <content_attribute id="social-audio">none</content_attribute>
    <content_attribute id="social-location">none</content_attribute>
    <content_attribute id="social-contacts">none</content_attribute>
    <content_attribute id="money-purchasing">none</content_attribute>
    <content_attribute id="money-gambling">none</content_attribute>
  </content_rating>
</component>
